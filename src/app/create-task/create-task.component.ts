import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';


interface ITask {
  content: string;
  date: string;
  complected: boolean;
}

@Component({
  selector: 'app-create-task',
  templateUrl: './create-task.component.html',
  styleUrls: ['./create-task.component.css']
})
export class CreateTaskComponent implements OnInit {

  contentTask = '';
  classActiveTitle = false;
  classActiveContent = false;

  constructor(public data: DataService) {
  }
  ngOnInit() {}

  createDate () {
    const date = new Date();
    const currentDate = date.getDate() + '.'
                        + +(date.getMonth() + 1) + '.'
                        + date.getFullYear();

    return currentDate;
  }


  createTask (form) {
    let task: ITask = {
      content: '',
      date: '',
      complected: false
    };
    task.content = this.contentTask;
    task.date = this.createDate();
    this.data.addTasks(task);
    task = null;
    this.contentTask = '';
  }
}
