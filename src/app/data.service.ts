
export class DataService  {

  constructor() {
    const loadedTasksStorage: string = localStorage.getItem('tasks');
      if (loadedTasksStorage) {
         this.initTasks();
      }
  }
  private tasks = [];

  delTask(id) {
    const loadedTasksStorage: string = localStorage.getItem('tasks');
    let tasks = JSON.parse(loadedTasksStorage);
    tasks.splice(id, 1);
    this.tasks = tasks;
    console.log(tasks);
    tasks = JSON.stringify(tasks);
    localStorage.setItem('tasks', tasks);
  }

  addTasks(task) {
    this.tasks.push(task);
    const tasksJSON: string = JSON.stringify(this.tasks);
    localStorage.setItem('tasks', tasksJSON);
    console.log(this.tasks);
  }

  initTasks() {
    const loadedTasksStorage: string = localStorage.getItem('tasks');
    const loadedTasksJSON: object[] = JSON.parse(loadedTasksStorage);
    if (loadedTasksJSON.length !== 0) {
      return this.tasks = loadedTasksJSON;
    }
  }

  togger(index) {
    const loadedTasksStorage: string = localStorage.getItem('tasks');
    let tasks = JSON.parse(loadedTasksStorage);
    tasks[index].complected = !tasks[index].complected;
    console.log(tasks);
    tasks = JSON.stringify(tasks);
    localStorage.setItem('tasks', tasks);
  }

  getTasks() {
    return this.tasks;
  }
}
