import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatInputModule, MatToolbarModule, MatIconModule } from '@angular/material';

@NgModule({
imports: [MatButtonModule,
          MatInputModule,
          MatToolbarModule,
          MatIconModule],
exports: [MatButtonModule,
          MatInputModule,
          MatToolbarModule,
          MatIconModule]
})

export class MaterialAppModule { }
