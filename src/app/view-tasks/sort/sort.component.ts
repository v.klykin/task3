import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-sort',
  templateUrl: './sort.component.html',
  styleUrls: ['./sort.component.css']
})
export class SortComponent implements OnInit {

  public text: string;

  @Output() sort = new EventEmitter<string>();
    onSort() {
        this.sort.emit(this.text.toLowerCase());
    }

  constructor() { }

  ngOnInit() {
    this.text = '';
  }

}
