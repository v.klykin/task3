import { Component, OnInit, DoCheck, Input} from '@angular/core';
import { DataService } from '../data.service';
import { filterQueryId } from '@angular/core/src/view/util';




@Component({
  selector: 'app-view-tasks',
  templateUrl: './view-tasks.component.html',
  styleUrls: ['./view-tasks.component.css']
})

export class ViewTasksComponent implements OnInit, DoCheck {
  private tasks = [];
  private date = [];
  private filterText: string;

  constructor (private data: DataService) { }

  ngOnInit() {
    this.tasks = this.data.getTasks();
  }

  ngDoCheck () {
    // this.tasks.forEach((item) => {
    //   if (this.todayDate() === item.date) {
    //     this.date.push('Сегодня');
    //   } else {
    //     this.date.push(item.date);
    //   }
    // });
  }

  todayDate () {
    const date = new Date();
    const currentDate = date.getDate() + '.'
                        + +(date.getMonth() + 1) + '.'
                        + date.getFullYear();
    return currentDate;
  }


  delete(task) {
    const loadedTasksStorage: string = localStorage.getItem('tasks');
    const forDeleteTasks = JSON.parse(loadedTasksStorage);
    forDeleteTasks.forEach((item, index) => {
      if (task.content === item.content &&
          task.date === item.date &&
          task.complected === item.complected) {
        console.log(index);
        this.data.delTask(index);
      }
    });
    this.tasks = this.data.getTasks();
    this.filter(this.filterText);
  }


  filter(text: string) {
    this.tasks = this.data.getTasks();
    this.tasks = this.tasks.filter(function(task) {
      if ((task.content.toLowerCase().indexOf(text) >= 0) ||
          (task.date.toLowerCase().indexOf(text) >= 0)) {
            return task;
        }
    });
    if (text.length === 0) {
      this.tasks = this.data.getTasks();
    }
    this.filterText = text;
  }

  togger(task, index: string) {
    const loadedTasksStorage: string = localStorage.getItem('tasks');
    const forToggerTasks = JSON.parse(loadedTasksStorage);
    forToggerTasks.forEach((item, i) => {
      if (task.content === item.content &&
          task.date === item.date &&
          task.complected === item.complected) {
          this.data.togger(i);
      }
    });
    this.tasks[index].complected = !this.tasks[index].complected;
  }

  sort (boolean, prop) {
    this.tasks = this.tasks.sort((a, b) => {
        if (prop) {
          const aLower = a.date.toLowerCase();
          const bLower = b.date.toLowerCase();
          if (boolean) {
            return bLower.localeCompare(aLower);
           }
           return aLower.localeCompare(bLower);
        } else {
          const aLower = a.content.toLowerCase();
          const bLower = b.content.toLowerCase();
          if (boolean) {
             return bLower.localeCompare(aLower);
          }
          return aLower.localeCompare(bLower);
        }
    });
  }
}
